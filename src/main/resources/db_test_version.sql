DROP DATABASE IF EXISTS my_first_blog;

CREATE DATABASE my_first_blog DEFAULT CHARACTER SET = 'utf8mb4';

USE my_first_blog;

CREATE TABLE category (
	id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	label VARCHAR(32) NOT NULL
);

DROP DATABASE IF EXISTS my_first_blog;

CREATE DATABASE my_first_blog;

USE my_first_blog;

DROP TABLE IF EXISTS category;

CREATE TABLE IF NOT EXISTS category (
	id TINYINT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
	label VARCHAR(64) NOT NULL
);

DROP TABLE IF EXISTS article;

CREATE TABLE IF NOT EXISTS article (
	id TINYINT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
	id_category TINYINT UNSIGNED NOT NULL,
	title VARCHAR(128) NOT NULL,
	date DATE NOT NULL,
	author VARCHAR(64) NOT NULL,
	content TEXT NOT NULL,
	CONSTRAINT FK_article_category FOREIGN KEY (id_category) REFERENCES category(id) ON DELETE CASCADE ON UPDATE CASCADE);

INSERT INTO
	category (label)
VALUES
	('Technology'),
	('Climate'),
	('Politics'),('Nature'),
	('Animals'),
	('Sport'),
	('Misc.');

INSERT INTO
	article(id_category, title, date, author, content)
VALUES
	(
		1,
		'Test article',
		'2022-04-10',
		'@Lymih',
		'Nunc commodo urna id erat ornare, vitae aliquet massa lacinia. Vivamus commodo faucibus purus, eu ultrices mi vestibulum vel. Aliquam efficitur, justo quis pharetra bibendum, lorem nibh auctor sapien, pellentesque tincidunt velit lorem a lectus. Etiam quis felis at purus mollis rhoncus ultricies tincidunt diam. Nulla hendrerit nibh in nulla interdum mollis. Sed ultrices quam vel eleifend varius. In vel ante et arcu ullamcorper mollis. Donec maximus vestibulum fermentum. Proin nulla neque, lobortis in semper eget, ullamcorper quis ipsum. Praesent vestibulum elit a quam placerat, sed rutrum tortor fringilla. Morbi dictum maximus dignissim. Sed in dapibus magna, eget molestie ante. Etiam nec ipsum eget tellus placerat cursus semper id odio. Fusce varius vel arcu non sodales. Nunc commodo urna id erat ornare, vitae aliquet massa lacinia. Vivamus commodo faucibus purus, eu ultrices mi vestibulum vel. Aliquam efficitur, justo quis pharetra bibendum, lorem nibh auctor sapien, pellentesque tincidunt velit lorem a lectus. Etiam quis felis at purus mollis rhoncus ultricies tincidunt diam. Nulla hendrerit nibh in nulla interdum mollis. Sed ultrices quam vel eleifend varius. In vel ante et arcu ullamcorper mollis. Donec maximus vestibulum fermentum. Proin nulla neque, lobortis in semper eget, ullamcorper quis ipsum. Praesent vestibulum elit a quam placerat, sed rutrum tortor fringilla. Morbi dictum maximus dignissim. Sed in dapibus magna, eget molestie ante. Etiam nec ipsum eget tellus placerat cursus semper id odio. Fusce varius vel arcu non sodales. Nunc commodo urna id erat ornare, vitae aliquet massa lacinia. Vivamus commodo faucibus purus, eu ultrices mi vestibulum vel. Aliquam efficitur, justo quis pharetra bibendum, lorem nibh auctor sapien, pellentesque tincidunt velit lorem a lectus. Etiam quis felis at purus mollis rhoncus ultricies tincidunt diam. Nulla hendrerit nibh in nulla interdum mollis. Sed ultrices quam vel eleifend varius. In vel ante et arcu ullamcorper mollis. Donec maximus vestibulum fermentum. Proin nulla neque, lobortis in semper eget, ullamcorper quis ipsum. Praesent vestibulum elit a quam placerat, sed rutrum tortor fringilla. Morbi dictum maximus dignissim. Sed in dapibus magna, eget molestie ante. Etiam nec ipsum eget tellus placerat cursus semper id odio. Fusce varius vel arcu non sodales. '
	);

INSERT INTO
	article(id_category, title, date, author, content)
VALUES
	(
		2,
		'Test article 2',
		'2022-04-10',
		'@Lymih',
		'Nunc commodo urna id erat ornare, vitae aliquet massa lacinia. Vivamus commodo faucibus purus, eu ultrices mi vestibulum vel. Aliquam efficitur, justo quis pharetra bibendum, lorem nibh auctor sapien, pellentesque tincidunt velit lorem a lectus. Etiam quis felis at purus mollis rhoncus ultricies tincidunt diam. Nulla hendrerit nibh in nulla interdum mollis. Sed ultrices quam vel eleifend varius. In vel ante et arcu ullamcorper mollis. Donec maximus vestibulum fermentum. Proin nulla neque, lobortis in semper eget, ullamcorper quis ipsum. Praesent vestibulum elit a quam placerat, sed rutrum tortor fringilla. Morbi dictum maximus dignissim. Sed in dapibus magna, eget molestie ante. Etiam nec ipsum eget tellus placerat cursus semper id odio. Fusce varius vel arcu non sodales. Nunc commodo urna id erat ornare, vitae aliquet massa lacinia. Vivamus commodo faucibus purus, eu ultrices mi vestibulum vel. Aliquam efficitur, justo quis pharetra bibendum, lorem nibh auctor sapien, pellentesque tincidunt velit lorem a lectus. Etiam quis felis at purus mollis rhoncus ultricies tincidunt diam. Nulla hendrerit nibh in nulla interdum mollis. Sed ultrices quam vel eleifend varius. In vel ante et arcu ullamcorper mollis. Donec maximus vestibulum fermentum. Proin nulla neque, lobortis in semper eget, ullamcorper quis ipsum. Praesent vestibulum elit a quam placerat, sed rutrum tortor fringilla. Morbi dictum maximus dignissim. Sed in dapibus magna, eget molestie ante. Etiam nec ipsum eget tellus placerat cursus semper id odio. Fusce varius vel arcu non sodales. Nunc commodo urna id erat ornare, vitae aliquet massa lacinia. Vivamus commodo faucibus purus, eu ultrices mi vestibulum vel. Aliquam efficitur, justo quis pharetra bibendum, lorem nibh auctor sapien, pellentesque tincidunt velit lorem a lectus. Etiam quis felis at purus mollis rhoncus ultricies tincidunt diam. Nulla hendrerit nibh in nulla interdum mollis. Sed ultrices quam vel eleifend varius. In vel ante et arcu ullamcorper mollis. Donec maximus vestibulum fermentum. Proin nulla neque, lobortis in semper eget, ullamcorper quis ipsum. Praesent vestibulum elit a quam placerat, sed rutrum tortor fringilla. Morbi dictum maximus dignissim. Sed in dapibus magna, eget molestie ante. Etiam nec ipsum eget tellus placerat cursus semper id odio. Fusce varius vel arcu non sodales. '
	);

INSERT INTO
	article(id_category, title, date, author, content)
VALUES
	(
		3,
		'Test article 3',
		'2022-04-12',
		'@Gastondu31',
		'Nunc commodo urna id erat ornare, vitae aliquet massa lacinia. Vivamus commodo faucibus purus, eu ultrices mi vestibulum vel. Aliquam efficitur, justo quis pharetra bibendum, lorem nibh auctor sapien, pellentesque tincidunt velit lorem a lectus. Etiam quis felis at purus mollis rhoncus ultricies tincidunt diam. Nulla hendrerit nibh in nulla interdum mollis. Sed ultrices quam vel eleifend varius. In vel ante et arcu ullamcorper mollis. Donec maximus vestibulum fermentum. Proin nulla neque, lobortis in semper eget, ullamcorper quis ipsum. Praesent vestibulum elit a quam placerat, sed rutrum tortor fringilla. Morbi dictum maximus dignissim. Sed in dapibus magna, eget molestie ante. Etiam nec ipsum eget tellus placerat cursus semper id odio. Fusce varius vel arcu non sodales. Nunc commodo urna id erat ornare, vitae aliquet massa lacinia. Vivamus commodo faucibus purus, eu ultrices mi vestibulum vel. Aliquam efficitur, justo quis pharetra bibendum, lorem nibh auctor sapien, pellentesque tincidunt velit lorem a lectus. Etiam quis felis at purus mollis rhoncus ultricies tincidunt diam. Nulla hendrerit nibh in nulla interdum mollis. Sed ultrices quam vel eleifend varius. In vel ante et arcu ullamcorper mollis. Donec maximus vestibulum fermentum. Proin nulla neque, lobortis in semper eget, ullamcorper quis ipsum. Praesent vestibulum elit a quam placerat, sed rutrum tortor fringilla. Morbi dictum maximus dignissim. Sed in dapibus magna, eget molestie ante. Etiam nec ipsum eget tellus placerat cursus semper id odio. Fusce varius vel arcu non sodales. Nunc commodo urna id erat ornare, vitae aliquet massa lacinia. Vivamus commodo faucibus purus, eu ultrices mi vestibulum vel. Aliquam efficitur, justo quis pharetra bibendum, lorem nibh auctor sapien, pellentesque tincidunt velit lorem a lectus. Etiam quis felis at purus mollis rhoncus ultricies tincidunt diam. Nulla hendrerit nibh in nulla interdum mollis. Sed ultrices quam vel eleifend varius. In vel ante et arcu ullamcorper mollis. Donec maximus vestibulum fermentum. Proin nulla neque, lobortis in semper eget, ullamcorper quis ipsum. Praesent vestibulum elit a quam placerat, sed rutrum tortor fringilla. Morbi dictum maximus dignissim. Sed in dapibus magna, eget molestie ante. Etiam nec ipsum eget tellus placerat cursus semper id odio. Fusce varius vel arcu non sodales. '
	);

INSERT INTO
	article(id_category, title, date, author, content)
VALUES
	(
		1,
		'Test article 4',
		'2022-04-15',
		'@Lymih',
		'Nunc commodo urna id erat ornare, vitae aliquet massa lacinia. Vivamus commodo faucibus purus, eu ultrices mi vestibulum vel. Aliquam efficitur, justo quis pharetra bibendum, lorem nibh auctor sapien, pellentesque tincidunt velit lorem a lectus. Etiam quis felis at purus mollis rhoncus ultricies tincidunt diam. Nulla hendrerit nibh in nulla interdum mollis. Sed ultrices quam vel eleifend varius. In vel ante et arcu ullamcorper mollis. Donec maximus vestibulum fermentum. Proin nulla neque, lobortis in semper eget, ullamcorper quis ipsum. Praesent vestibulum elit a quam placerat, sed rutrum tortor fringilla. Morbi dictum maximus dignissim. Sed in dapibus magna, eget molestie ante. Etiam nec ipsum eget tellus placerat cursus semper id odio. Fusce varius vel arcu non sodales. Nunc commodo urna id erat ornare, vitae aliquet massa lacinia. Vivamus commodo faucibus purus, eu ultrices mi vestibulum vel. Aliquam efficitur, justo quis pharetra bibendum, lorem nibh auctor sapien, pellentesque tincidunt velit lorem a lectus. Etiam quis felis at purus mollis rhoncus ultricies tincidunt diam. Nulla hendrerit nibh in nulla interdum mollis. Sed ultrices quam vel eleifend varius. In vel ante et arcu ullamcorper mollis. Donec maximus vestibulum fermentum. Proin nulla neque, lobortis in semper eget, ullamcorper quis ipsum. Praesent vestibulum elit a quam placerat, sed rutrum tortor fringilla. Morbi dictum maximus dignissim. Sed in dapibus magna, eget molestie ante. Etiam nec ipsum eget tellus placerat cursus semper id odio. Fusce varius vel arcu non sodales. Nunc commodo urna id erat ornare, vitae aliquet massa lacinia. Vivamus commodo faucibus purus, eu ultrices mi vestibulum vel. Aliquam efficitur, justo quis pharetra bibendum, lorem nibh auctor sapien, pellentesque tincidunt velit lorem a lectus. Etiam quis felis at purus mollis rhoncus ultricies tincidunt diam. Nulla hendrerit nibh in nulla interdum mollis. Sed ultrices quam vel eleifend varius. In vel ante et arcu ullamcorper mollis. Donec maximus vestibulum fermentum. Proin nulla neque, lobortis in semper eget, ullamcorper quis ipsum. Praesent vestibulum elit a quam placerat, sed rutrum tortor fringilla. Morbi dictum maximus dignissim. Sed in dapibus magna, eget molestie ante. Etiam nec ipsum eget tellus placerat cursus semper id odio. Fusce varius vel arcu non sodales. '
	);