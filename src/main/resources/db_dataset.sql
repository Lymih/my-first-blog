USE my_first_blog;
INSERT INTO
	category (label)
VALUES
	('Technology'),
	('Climate'),
	('Politics'),('Nature'),
	('Animals'),
	('Sport'),
	('Misc.');

INSERT INTO
	article(id_category, title, date, author, content)
VALUES
	(
		1,
		'Test article',
		'2022-04-10',
		'@Lymih',
		'Nunc commodo urna id erat ornare, vitae aliquet massa lacinia. Vivamus commodo faucibus purus, eu ultrices mi vestibulum vel. Aliquam efficitur, justo quis pharetra bibendum, lorem nibh auctor sapien, pellentesque tincidunt velit lorem a lectus. Etiam quis felis at purus mollis rhoncus ultricies tincidunt diam. Nulla hendrerit nibh in nulla interdum mollis. Sed ultrices quam vel eleifend varius. In vel ante et arcu ullamcorper mollis. Donec maximus vestibulum fermentum. Proin nulla neque, lobortis in semper eget, ullamcorper quis ipsum. Praesent vestibulum elit a quam placerat, sed rutrum tortor fringilla. Morbi dictum maximus dignissim. Sed in dapibus magna, eget molestie ante. Etiam nec ipsum eget tellus placerat cursus semper id odio. Fusce varius vel arcu non sodales. Nunc commodo urna id erat ornare, vitae aliquet massa lacinia. Vivamus commodo faucibus purus, eu ultrices mi vestibulum vel. Aliquam efficitur, justo quis pharetra bibendum, lorem nibh auctor sapien, pellentesque tincidunt velit lorem a lectus. Etiam quis felis at purus mollis rhoncus ultricies tincidunt diam. Nulla hendrerit nibh in nulla interdum mollis. Sed ultrices quam vel eleifend varius. In vel ante et arcu ullamcorper mollis. Donec maximus vestibulum fermentum. Proin nulla neque, lobortis in semper eget, ullamcorper quis ipsum. Praesent vestibulum elit a quam placerat, sed rutrum tortor fringilla. Morbi dictum maximus dignissim. Sed in dapibus magna, eget molestie ante. Etiam nec ipsum eget tellus placerat cursus semper id odio. Fusce varius vel arcu non sodales. Nunc commodo urna id erat ornare, vitae aliquet massa lacinia. Vivamus commodo faucibus purus, eu ultrices mi vestibulum vel. Aliquam efficitur, justo quis pharetra bibendum, lorem nibh auctor sapien, pellentesque tincidunt velit lorem a lectus. Etiam quis felis at purus mollis rhoncus ultricies tincidunt diam. Nulla hendrerit nibh in nulla interdum mollis. Sed ultrices quam vel eleifend varius. In vel ante et arcu ullamcorper mollis. Donec maximus vestibulum fermentum. Proin nulla neque, lobortis in semper eget, ullamcorper quis ipsum. Praesent vestibulum elit a quam placerat, sed rutrum tortor fringilla. Morbi dictum maximus dignissim. Sed in dapibus magna, eget molestie ante. Etiam nec ipsum eget tellus placerat cursus semper id odio. Fusce varius vel arcu non sodales. '
	);

  INSERT INTO
	article(id_category, title, date, author, content)
VALUES
	(
		2,
		'Test article 2',
		'2022-04-10',
		'@Lymih',
		'Nunc commodo urna id erat ornare, vitae aliquet massa lacinia. Vivamus commodo faucibus purus, eu ultrices mi vestibulum vel. Aliquam efficitur, justo quis pharetra bibendum, lorem nibh auctor sapien, pellentesque tincidunt velit lorem a lectus. Etiam quis felis at purus mollis rhoncus ultricies tincidunt diam. Nulla hendrerit nibh in nulla interdum mollis. Sed ultrices quam vel eleifend varius. In vel ante et arcu ullamcorper mollis. Donec maximus vestibulum fermentum. Proin nulla neque, lobortis in semper eget, ullamcorper quis ipsum. Praesent vestibulum elit a quam placerat, sed rutrum tortor fringilla. Morbi dictum maximus dignissim. Sed in dapibus magna, eget molestie ante. Etiam nec ipsum eget tellus placerat cursus semper id odio. Fusce varius vel arcu non sodales. Nunc commodo urna id erat ornare, vitae aliquet massa lacinia. Vivamus commodo faucibus purus, eu ultrices mi vestibulum vel. Aliquam efficitur, justo quis pharetra bibendum, lorem nibh auctor sapien, pellentesque tincidunt velit lorem a lectus. Etiam quis felis at purus mollis rhoncus ultricies tincidunt diam. Nulla hendrerit nibh in nulla interdum mollis. Sed ultrices quam vel eleifend varius. In vel ante et arcu ullamcorper mollis. Donec maximus vestibulum fermentum. Proin nulla neque, lobortis in semper eget, ullamcorper quis ipsum. Praesent vestibulum elit a quam placerat, sed rutrum tortor fringilla. Morbi dictum maximus dignissim. Sed in dapibus magna, eget molestie ante. Etiam nec ipsum eget tellus placerat cursus semper id odio. Fusce varius vel arcu non sodales. Nunc commodo urna id erat ornare, vitae aliquet massa lacinia. Vivamus commodo faucibus purus, eu ultrices mi vestibulum vel. Aliquam efficitur, justo quis pharetra bibendum, lorem nibh auctor sapien, pellentesque tincidunt velit lorem a lectus. Etiam quis felis at purus mollis rhoncus ultricies tincidunt diam. Nulla hendrerit nibh in nulla interdum mollis. Sed ultrices quam vel eleifend varius. In vel ante et arcu ullamcorper mollis. Donec maximus vestibulum fermentum. Proin nulla neque, lobortis in semper eget, ullamcorper quis ipsum. Praesent vestibulum elit a quam placerat, sed rutrum tortor fringilla. Morbi dictum maximus dignissim. Sed in dapibus magna, eget molestie ante. Etiam nec ipsum eget tellus placerat cursus semper id odio. Fusce varius vel arcu non sodales. '
	);

   INSERT INTO
	article(id_category, title, date, author, content)
VALUES
	(
		3,
		'Test article 3',
		'2022-04-12',
		'@Gastondu31',
		'Nunc commodo urna id erat ornare, vitae aliquet massa lacinia. Vivamus commodo faucibus purus, eu ultrices mi vestibulum vel. Aliquam efficitur, justo quis pharetra bibendum, lorem nibh auctor sapien, pellentesque tincidunt velit lorem a lectus. Etiam quis felis at purus mollis rhoncus ultricies tincidunt diam. Nulla hendrerit nibh in nulla interdum mollis. Sed ultrices quam vel eleifend varius. In vel ante et arcu ullamcorper mollis. Donec maximus vestibulum fermentum. Proin nulla neque, lobortis in semper eget, ullamcorper quis ipsum. Praesent vestibulum elit a quam placerat, sed rutrum tortor fringilla. Morbi dictum maximus dignissim. Sed in dapibus magna, eget molestie ante. Etiam nec ipsum eget tellus placerat cursus semper id odio. Fusce varius vel arcu non sodales. Nunc commodo urna id erat ornare, vitae aliquet massa lacinia. Vivamus commodo faucibus purus, eu ultrices mi vestibulum vel. Aliquam efficitur, justo quis pharetra bibendum, lorem nibh auctor sapien, pellentesque tincidunt velit lorem a lectus. Etiam quis felis at purus mollis rhoncus ultricies tincidunt diam. Nulla hendrerit nibh in nulla interdum mollis. Sed ultrices quam vel eleifend varius. In vel ante et arcu ullamcorper mollis. Donec maximus vestibulum fermentum. Proin nulla neque, lobortis in semper eget, ullamcorper quis ipsum. Praesent vestibulum elit a quam placerat, sed rutrum tortor fringilla. Morbi dictum maximus dignissim. Sed in dapibus magna, eget molestie ante. Etiam nec ipsum eget tellus placerat cursus semper id odio. Fusce varius vel arcu non sodales. Nunc commodo urna id erat ornare, vitae aliquet massa lacinia. Vivamus commodo faucibus purus, eu ultrices mi vestibulum vel. Aliquam efficitur, justo quis pharetra bibendum, lorem nibh auctor sapien, pellentesque tincidunt velit lorem a lectus. Etiam quis felis at purus mollis rhoncus ultricies tincidunt diam. Nulla hendrerit nibh in nulla interdum mollis. Sed ultrices quam vel eleifend varius. In vel ante et arcu ullamcorper mollis. Donec maximus vestibulum fermentum. Proin nulla neque, lobortis in semper eget, ullamcorper quis ipsum. Praesent vestibulum elit a quam placerat, sed rutrum tortor fringilla. Morbi dictum maximus dignissim. Sed in dapibus magna, eget molestie ante. Etiam nec ipsum eget tellus placerat cursus semper id odio. Fusce varius vel arcu non sodales. '
	);
   INSERT INTO
	article(id_category, title, date, author, content)
VALUES
	(
		1,
		'Test article 4',
		'2022-04-15',
		'@Lymih',
		'Nunc commodo urna id erat ornare, vitae aliquet massa lacinia. Vivamus commodo faucibus purus, eu ultrices mi vestibulum vel. Aliquam efficitur, justo quis pharetra bibendum, lorem nibh auctor sapien, pellentesque tincidunt velit lorem a lectus. Etiam quis felis at purus mollis rhoncus ultricies tincidunt diam. Nulla hendrerit nibh in nulla interdum mollis. Sed ultrices quam vel eleifend varius. In vel ante et arcu ullamcorper mollis. Donec maximus vestibulum fermentum. Proin nulla neque, lobortis in semper eget, ullamcorper quis ipsum. Praesent vestibulum elit a quam placerat, sed rutrum tortor fringilla. Morbi dictum maximus dignissim. Sed in dapibus magna, eget molestie ante. Etiam nec ipsum eget tellus placerat cursus semper id odio. Fusce varius vel arcu non sodales. Nunc commodo urna id erat ornare, vitae aliquet massa lacinia. Vivamus commodo faucibus purus, eu ultrices mi vestibulum vel. Aliquam efficitur, justo quis pharetra bibendum, lorem nibh auctor sapien, pellentesque tincidunt velit lorem a lectus. Etiam quis felis at purus mollis rhoncus ultricies tincidunt diam. Nulla hendrerit nibh in nulla interdum mollis. Sed ultrices quam vel eleifend varius. In vel ante et arcu ullamcorper mollis. Donec maximus vestibulum fermentum. Proin nulla neque, lobortis in semper eget, ullamcorper quis ipsum. Praesent vestibulum elit a quam placerat, sed rutrum tortor fringilla. Morbi dictum maximus dignissim. Sed in dapibus magna, eget molestie ante. Etiam nec ipsum eget tellus placerat cursus semper id odio. Fusce varius vel arcu non sodales. Nunc commodo urna id erat ornare, vitae aliquet massa lacinia. Vivamus commodo faucibus purus, eu ultrices mi vestibulum vel. Aliquam efficitur, justo quis pharetra bibendum, lorem nibh auctor sapien, pellentesque tincidunt velit lorem a lectus. Etiam quis felis at purus mollis rhoncus ultricies tincidunt diam. Nulla hendrerit nibh in nulla interdum mollis. Sed ultrices quam vel eleifend varius. In vel ante et arcu ullamcorper mollis. Donec maximus vestibulum fermentum. Proin nulla neque, lobortis in semper eget, ullamcorper quis ipsum. Praesent vestibulum elit a quam placerat, sed rutrum tortor fringilla. Morbi dictum maximus dignissim. Sed in dapibus magna, eget molestie ante. Etiam nec ipsum eget tellus placerat cursus semper id odio. Fusce varius vel arcu non sodales. '
	);