package co.simplon.promo18.myfirstblog.repository;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import co.simplon.promo18.myfirstblog.entity.Article;
import co.simplon.promo18.myfirstblog.entity.Category;

@Repository
public class ArticleRepository {
  @Autowired
  private DataSource dataSource;

  public List<Article> findAll() {
    List<Article> list = new ArrayList<>();

    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement(
          "SELECT * FROM article LEFT JOIN category ON article.id_category = category.id");
      ResultSet rs = stmt.executeQuery();

      while (rs.next()) {
        Category category = new Category(rs.getInt("category.id"), rs.getString("category.label"));
        Article article = new Article(rs.getInt("id"), category, rs.getString("title"),
            rs.getDate("date").toLocalDate(), rs.getString("author"), rs.getString("content"));
        list.add(article);
      }



    } catch (SQLException e) {
      e.printStackTrace();
      throw new RuntimeException("Database access error");
    }
    return list;
  }

  public Article findById(int id) {
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement(
          "SELECT * FROM article LEFT JOIN category ON article.id_category = category.id WHERE article.id =?");
      stmt.setInt(1, id);
      ResultSet rs = stmt.executeQuery();
      if (rs.next()) {
        Category category = new Category(rs.getInt("category.id"), rs.getString("category.label"));
        Article article = new Article(rs.getInt("id"), category, rs.getString("title"),
            rs.getDate("date").toLocalDate(), rs.getString("author"), rs.getString("content"));
        return article;
      }
    } catch (SQLException e) {
      e.printStackTrace();
      throw new RuntimeException("Database access error");
    }
    return null;
  }

  public List<Article> findByIdCategory(int id) {
    List<Article> list = new ArrayList<>();
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement(
          "SELECT *, category.label AS cat_label  FROM article LEFT JOIN category ON article.id_category = category.id WHERE id_category =?");
      stmt.setInt(1, id);
      ResultSet rs = stmt.executeQuery();
      Category category = null;
      while (rs.next()) {
        if (category == null)
          category = new Category(rs.getInt("id_category"), rs.getString("cat_label"));
        Article article = new Article(rs.getInt("id"), category, rs.getString("title"),
            rs.getDate("date").toLocalDate(), rs.getString("author"), rs.getString("content"));
        list.add(article);

      }
    } catch (SQLException e) {
      e.printStackTrace();
      throw new RuntimeException("Database access error");
    }
    return list;
  }

  public Article save(Article article) {

    try (Connection connection = dataSource.getConnection()) {

      PreparedStatement stmt = connection.prepareStatement(
          "INSERT INTO article(id_category, title, date, author, content) VALUES (?,?,?,?,?)",
          PreparedStatement.RETURN_GENERATED_KEYS);
      stmt.setInt(1, article.getCategory().getId());
      stmt.setString(2, article.getTitle());
      stmt.setDate(3, Date.valueOf(article.getDate()));
      stmt.setString(4, article.getAuthor());
      stmt.setString(5, article.getContent());
      stmt.executeUpdate();

      ResultSet rs = stmt.getGeneratedKeys();

      if (rs.next()) {
        article.setId(rs.getInt(1));
      }
      return article;
    } catch (SQLException e) {
      e.printStackTrace();
      throw new RuntimeException("Database access error");
    }
  }

  public boolean update(Article article) {
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement(
          "UPDATE article SET id_category= ?, title= ?, date= ?, author=?, content=? WHERE id= ?");
      stmt.setInt(1, article.getCategory().getId());
      stmt.setString(2, article.getTitle());
      stmt.setDate(3, Date.valueOf(article.getDate()));
      stmt.setString(4, article.getAuthor());
      stmt.setString(5, article.getContent());
      stmt.setInt(6, article.getId());
      return (stmt.executeUpdate() == 1);

    } catch (SQLException e) {
      e.printStackTrace();
      throw new RuntimeException("Database access error");
    }
  }



  public boolean delete(int id) {
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement("DELETE FROM article WHERE id=?");
      stmt.setInt(1, id);

      return (stmt.executeUpdate() == 1);
    } catch (SQLException e) {
      e.printStackTrace();
      throw new RuntimeException("Database access error");
    }
  }
}
