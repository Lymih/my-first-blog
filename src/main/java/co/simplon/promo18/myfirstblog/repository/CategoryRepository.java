package co.simplon.promo18.myfirstblog.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import co.simplon.promo18.myfirstblog.entity.Category;

@Repository
public class CategoryRepository {

  @Autowired
  private DataSource dataSource;

  public List<Category> findAll() {
    List<Category> list = new ArrayList<>();
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement("SELECT * FROM category");
      ResultSet rs = stmt.executeQuery();
      while (rs.next()) {
        Category category = new Category(rs.getInt("id"), rs.getString("label"));
        list.add(category);
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return list;
  }

  public Category findById(int id) {
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement("SELECT * FROM category WHERE id=?");
      stmt.setInt(1, id);
      ResultSet rs = stmt.executeQuery();
      if (rs.next()) {
        Category category = new Category(rs.getInt("id"), rs.getString("label"));
        return category;
      }
    } catch (SQLException e) {
      e.printStackTrace();
      throw new RuntimeException("Database access error");
    }
    return null;
  }

  public Category save(Category category){
   
    try( Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement("INSERT INTO category (label) VALUES (?)",
      PreparedStatement.RETURN_GENERATED_KEYS);
      stmt.setString(1,category.getLabel());
      stmt.executeUpdate();
      ResultSet rs = stmt.getGeneratedKeys();
      if (rs.next()){
        category.setId(rs.getInt(1));
      }
      return category;
    } catch (SQLException e) {
      e.printStackTrace();
      throw new RuntimeException("Database access error");
    }
  }
  public boolean delete(int id){
    try(Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement("DELETE FROM category WHERE id=?");
      stmt.setInt(1,id);
     
      return ( stmt.executeUpdate()==1);
    } catch (SQLException e) {
  
      e.printStackTrace();
      throw new RuntimeException("Database access error");
    }
    
  }

  public boolean update (Category category){
    try(Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement("UPDATE category SET label=? WHERE id=?");
      stmt.setString(1, category.getLabel());
      stmt.setInt(2,category.getId());
      return (stmt.executeUpdate()==1);
    } catch (SQLException e) {
      throw new RuntimeException("Database access error");
    }
    
  }
}
