package co.simplon.promo18.myfirstblog.controller;

import java.util.List;
import javax.validation.Valid;
import javax.validation.constraints.Min;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import co.simplon.promo18.myfirstblog.entity.Category;
import co.simplon.promo18.myfirstblog.repository.CategoryRepository;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@Validated
@RequestMapping(value="api/category")
public class CategoryController {

  @Autowired
  CategoryRepository repo;

  @GetMapping
  public List<Category>getAll(){
    return repo.findAll();
  }
  @GetMapping("/{id}")
  public Category getOne(@PathVariable @Min(1)int id){
    Category category = repo.findById(id);
   if(category==null) throw new ResponseStatusException(HttpStatus.NOT_FOUND);
     return category;
    }

  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  public Category post(@Valid @RequestBody Category category){
    repo.save(category);
    return category;
  }
  @PutMapping("/{id}")
public Category put(@Valid @RequestBody Category category, @PathVariable @Min(1) int id){
  if(id!=category.getId()) throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
  if(!repo.update(category))  throw new ResponseStatusException(HttpStatus.NOT_FOUND);
  
  return repo.findById(category.getId());
}
@DeleteMapping("/{id}")
@ResponseStatus(HttpStatus.NO_CONTENT)
public void delete(@PathVariable @Min(1) int id){
  if(!repo.delete(id)) throw new ResponseStatusException(HttpStatus.NOT_FOUND);
}

}
