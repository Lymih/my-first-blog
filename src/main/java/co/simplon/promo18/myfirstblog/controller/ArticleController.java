package co.simplon.promo18.myfirstblog.controller;

import java.util.List;
import javax.validation.Valid;
import javax.validation.constraints.Min;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import co.simplon.promo18.myfirstblog.entity.Article;
import co.simplon.promo18.myfirstblog.repository.ArticleRepository;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@Validated
@RequestMapping(value = "api/article")
public class ArticleController {

  @Autowired
  ArticleRepository repo;

  @GetMapping
  public List<Article> getAll() {
    return repo.findAll();
  }

  @GetMapping("category/{id}")
  public List<Article> getByCategory(@PathVariable @Min(1) int id) {

    return repo.findByIdCategory(id);
  }

  @GetMapping("/{id}")
  public Article getOne(@PathVariable @Min(1) int id) {
    Article article = repo.findById(id);
    if (article == null)
      throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    return article;
  }

  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  public Article post(@Valid @RequestBody Article article) {
    repo.save(article);
    return article;
  }

  @PutMapping("/{id}")
  public Article put(@Valid @RequestBody Article article, @PathVariable @Min(1) int id) {
    if (id != article.getId())
      throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
    if (!repo.update(article))
      throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    return repo.findById(article.getId());
  }

  @PatchMapping("/{id}")
  public Article patch(@RequestBody Article article, @PathVariable @Min(1) int id) {
    Article patchedArticle = repo.findById(id);
    if (patchedArticle == null)
      throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    if (article.getCategory() != null)
      patchedArticle.setCategory(article.getCategory());
    if (article.getTitle() != null)
      patchedArticle.setTitle(article.getTitle());
    if (article.getAuthor() != null)
      patchedArticle.setAuthor(article.getAuthor());
    if (article.getContent() != null)
      patchedArticle.setContent(article.getContent());
    patchedArticle.setDate(article.getDate());
    repo.update(patchedArticle);

    return repo.findById(patchedArticle.getId());
  }

  @DeleteMapping("/{id}")
  @ResponseStatus (HttpStatus.NO_CONTENT)
  public void delete(@PathVariable @Min(1) int id){
    if (!repo.delete(id)){
      throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    }
  }
}
