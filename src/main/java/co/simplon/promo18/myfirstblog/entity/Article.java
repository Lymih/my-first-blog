package co.simplon.promo18.myfirstblog.entity;

import java.time.LocalDate;
import javax.validation.constraints.Size;

public class Article {
  private int id;
  private Category category;
  @Size(min=3,max=24)
  private String title;
  private LocalDate date;
  @Size(min=4,max=24)
  private String author;
  @Size(min=3,max=5000)
  private String content;

  public Article() {}
  
  public Article(Category category, String title, LocalDate date, String author, String content) {
    this.category = category;
    this.title = title;
    this.date = date;
    this.author = author;
    this.content = content;
  }
  public Article(int id, Category category, String title, LocalDate date, String author,
      String content) {
    this.id = id;
    this.category = category;
    this.title = title;
    this.date = date;
    this.author = author;
    this.content = content;
  }
  public int getId() {
    return id;
  }
  public void setId(int id) {
    this.id = id;
  }
  public Category getCategory() {
    return category;
  }
  public void setCategory(Category category) {
    this.category = category;
  }
  public String getTitle() {
    return title;
  }
  public void setTitle(String title) {
    this.title = title;
  }
  public LocalDate getDate() {
    return date;
  }
  public void setDate(LocalDate date) {
    this.date = date;
  }
  public String getAuthor() {
    return author;
  }
  public void setAuthor(String author) {
    this.author = author;
  }
  public String getContent() {
    return content;
  }
  public void setContent(String content) {
    this.content = content;
  }

}
  