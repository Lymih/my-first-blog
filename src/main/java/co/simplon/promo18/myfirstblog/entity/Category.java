package co.simplon.promo18.myfirstblog.entity;


import javax.validation.constraints.Size;

public class Category {

  private int id;

 @Size(min=3,max=24)
  private String label;

  public Category() {

  }

  public Category(String label) {
    this.label = label;
  }

  public Category(int id, String label) {
    this.id = id;
    this.label = label;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getLabel() {
    return label;
  }

  public void setLabel(String label) {
    this.label = label;
  }

}
