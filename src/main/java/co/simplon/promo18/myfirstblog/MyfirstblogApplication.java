package co.simplon.promo18.myfirstblog;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MyfirstblogApplication {

	public static void main(String[] args) {
		SpringApplication.run(MyfirstblogApplication.class, args);
	}

}
