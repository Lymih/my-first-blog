package co.simplon.promo18.myfirstblog.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import co.simplon.promo18.myfirstblog.entity.Category;

@SpringBootTest
@Sql("/db_test_version.sql")
public class CategoryRepositoryTest {

  @Autowired
  CategoryRepository repo;

  @Test
  void testDeleteById() {
  repo.delete(3);
  }

  @Test
  void testFindAll() {
    List <Category> result = repo.findAll();
    assertEquals(7, result.size());

  }

  @Test
  void testFindById() {
    Category category = repo.findById(3);
    assertEquals("Politics", category.getLabel());
  }

  @Test
  void testSave() {
    Category category = new Category("Test category");
    repo.save(category);

  }

  @Test
  void testUpdate() {
    Category category= new Category(1,"Test label");
    repo.update(category);

  }
}
