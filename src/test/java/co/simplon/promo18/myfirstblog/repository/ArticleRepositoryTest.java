package co.simplon.promo18.myfirstblog.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import java.time.LocalDate;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import co.simplon.promo18.myfirstblog.entity.Article;
import co.simplon.promo18.myfirstblog.entity.Category;

@SpringBootTest
@Sql("/db_test_version.sql")
public class ArticleRepositoryTest {
  @Autowired
  ArticleRepository repo;

  @Test
  void testFindAll() {

    List<Article> result = repo.findAll();
    assertEquals(4, result.size());

  }

  @Test
  void testFindById() {
    Article article = repo.findById(1);
    assertEquals("Test article", article.getTitle());

  }

  @Test
  void testSave() {
    Category category = new Category(5, "test");
    Article article = new Article(category, "Test save article", LocalDate.of(2022, 05, 01),
        "@Lymih", "blablabla");
    repo.save(article);
  }

@Test
  void testUpdate() {
    Category category = new Category(3, "blabla");
    Article article = new Article(1, category, "Test update article", LocalDate.of(2022, 04, 01),
        "@Gastondu31", "updated content");
     repo.update(article);
  }



  @Test
  void testFindByCategory() {
    Category category = new Category(1,"blabla");
    List<Article> result  = repo.findByIdCategory(category.getId());
    assertEquals(2, result.size());
  }


  @Test
  void testDelete() {
    repo.delete(4);

  }


}
